from rply import LexerGenerator

lg = LexerGenerator()
lg.ignore(r"\s+")
lg.add("OPEN_CURLY_PAREN", r"\{")
lg.add("CLOSE_CURLY_PAREN", r"\}")
lg.add("OPEN_SQUARE_PAREN", r"\[")
lg.add("CLOSE_SQUARE_PAREN", r"\]")
lg.add("OPEN_PAREN", r"\(")
lg.add("CLOSE_PAREN", r"\)")
lg.add('COLON', r":")
lg.add("BOOLEAN", r"\b(true|false)\b")
lg.add("KEYWORD", r"\b(let|fn|defn)\b")
lg.add("IDENTIFIER", r"[a-zA-Z_][a-zA-Z_0-9]*")
lg.add("STRING", r"\'[a-zA-Z]+\'")
lg.add("OPERATOR", r"\+|-|\*|\/|!=|==|and|or")
lg.add("NUMBER", r"\d+")

tokens = [rule.name for rule in lg.rules ]

lexer = lg.build()
tokenize = lambda source: lexer.lex(source)
tokenList = lambda source: [token.gettokentype() for token in tokenize(source)]