from Parser import parse
from Scope import *

class Interpreter(object):
    def __init__(self):
        pass

    @staticmethod
    def evaluate(source, scope=None):
        if scope is None:
           scope = Scope() 

        ast = parse(source)
        result = ast.visit(scope=scope)
        return (result, scope)

    @classmethod
    def repl(cls):
        scope = Scope()
        while True:
            try:
                local_source = input('awe> ')
                result, scope = cls.evaluate(local_source, scope)
                print ( scope )
                print ( result )
            except Exception as e:
                print(e)
                continue

if __name__ == '__main__':
    Interpreter.repl()