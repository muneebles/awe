from Scope import Scope, FunctionLookupTree
# -----------------------------------------------------------------------------
# Base Abstract Syntax Tree node
# -----------------------------------------------------------------------------
# Requires the __visit__ function to be implemented
# for AST evaluation. I prefer having the definition
# live with the AST Node subclass than in a separate
# location.
# -----------------------------------------------------------------------------
class AstNode(object):
    # def __init__(self):
    #     pass

    def __eq__(self, other):
        return self.__repr__() == other.__repr__()

    def visit(self, scope):
        # Call the __visit__function if it exists
        # If it does not exist, raise an exception to implement it
        visitor_func = getattr(self, '__visit__', self.generic_visit)
        return visitor_func(scope=scope)

    def generic_visit(self, scope):
        errorMsg = 'No __visit__ method found on class {className}. Please implement for this subclass'.format(
            className=type(self).__name__
        )
        raise NotImplementedError(errorMsg)


# -----------------------------------------------------------------------------
# Literal objects that can be evaluated immediately
# -----------------------------------------------------------------------------
class Literal(AstNode):
    # def __init__(self):
    #     super().__init__()

    def __eq__(self, other):
        return self.value == other.value

    def __repr__(self):
        return type(self).__name__ + ': ' + str(self.value)

    def __visit__(self, scope):
        return self.value

class StringLiteral(Literal):
    def __init__(self, value):
        super().__init__()
        self.value = str(value)

class Operator(StringLiteral):
    pass

class Keyword(StringLiteral):
    pass

class Boolean(StringLiteral):
    def __visit__(self, scope):
        return {
           'true': True,
           'false': False
        }[self.value]

class Identifier(AstNode):
    def __init__(self, value):
        super().__init__()
        self.value = str(value)

    def __eq__(self, other):
        return self.value == other.value

    def __repr__(self):
        return type(self).__name__ + ': ' + str(self.value)

    def __visit__(self, scope):
        value = scope[self.value]
        if type(value) == ExpressionList:
            return value.visit(scope=scope)
        return value

class IntegerLiteral(Literal):
    def __init__(self, value):
        super().__init__()
        self.value = int(value)

class ArrayLiteral(Literal):
    def __init__(self):
        super().__init__()
        self.value = [] 

class DictionaryLiteral(Literal):
    def __init__(self, dictionary={}):
        super().__init__()
        self.value = dictionary

class DictionaryKeyValuePairList(AstNode):
    def __init__(self, pairs=[]):
        super().__init__()
        self.pairs = pairs

class DictionaryKeyValuePair(AstNode):
    def __init__(self, key, value):
        super().__init__()
        self.key = key
        self.value = value
    

# class FloatLiteral(Literal):
#     def __init__(self):
#         super().__init__()

# class RegularExpressionLiteral(Literal):
#     def __init__(self):
#         super().__init__()

# -----------------------------------------------------------------------------
# Expressions can be evaluated eagerly,
# but be careful not to modify program state in order
# to check for correctness
# -----------------------------------------------------------------------------

def get_default(some_value, default_value):
    return default_value if some_value is None else some_value

class Expression(AstNode):
    def __init__(self, function=None, arguments=None, parameters=None):
        super().__init__()
        self.function = get_default(function, Identifier('identity'))
        self.arguments = get_default(arguments, [])
        self.parameters = get_default(parameters, [])

    def __repr__(self):
        return self.function.__repr__() + ','.join([arg.__repr__() for arg in self.arguments])

    def __visit__(self, scope):
        function_identifier = self.function.value

        if function_identifier == 'identity':
            return self.arguments[0].visit(scope)
        elif type(self.function) == Keyword:
            if function_identifier == 'defn':
                for function_definition in self.arguments[1:]:
                    function_definition.identifier = self.arguments[0]
                    # Define function goes hand in hand with function lookup
                    scope.define_function(function_definition)
        else:
            # Find a way to delay the evaluation of arguments
            # if necessary. 
            if(function_identifier == 'if'):
                if(self.arguments[0].visit(scope=scope)):
                    return self.arguments[1].visit(scope=scope)
                else:
                    return self.arguments[2].visit(scope=scope)

            # Evaluate arguments of expression
            function_call_arguments = [arg.visit(scope=scope) for arg in self.arguments]

            # Define function goes hand in hand with function lookup
            function_instance = scope.lookup_function(function_identifier, function_call_arguments)
            if function_instance is not None:
                child_scope = scope.create_child()
                return function_instance.visit(scope=child_scope)
    
class ExpressionList(AstNode):
    def __init__(self, expressions=None):
        super().__init__()
        self.expressions = get_default(expressions, [])

    def __repr__(self):
        return ','.join([expr.__repr__() for expr in self.expressions])

    def __visit__(self, scope):
        # evaluate all except last AST node
        for intermediate in self.expressions[:-1]:
            intermediate.visit(scope)
        
        # Last AST node gets returned by default
        return self.expressions[-1].visit(scope)

# -----------------------------------------------------------------------------
# Definitions
# function, types, modules, etc.
# -----------------------------------------------------------------------------
class Function(AstNode):
    def __init__(self, expressions=None, parameters=None):
        super().__init__()
        self.parameters = get_default(parameters, [])
        self.expressions = ExpressionList(
            expressions=get_default(expressions, [])
        )

    def __visit__(self, scope):
        return self.expressions.visit(scope=scope)


