import operator
from functools import reduce
import AbstractSyntaxTree as AST

def get_default(some_value, default_value):
    return default_value if some_value is None else some_value

class FunctionLookupNode(object):
    def __init__(self, parameter=None, parent_node=None, expressions=None):
        self.parameter = parameter
        self.parent = parent_node
        self.children = []
        self.expressions = expressions

        if parent_node is not None:
            parent_node.add_child(self)
    
    def __eq__(self, other):
        eq_type = type(self.parameter) == type(other.parameter)
        eq_value = self.parameter.value == other.parameter.value
        return eq_type and eq_value

    def __lt__(self, other):
        # Set precedence
        precedence = [AST.Literal, AST.Identifier]

        # Map to whether the params are subclasses
        self_list = [isinstance(self.parameter, t) for t in precedence] 
        other_list = [isinstance(other.parameter, t) for t in precedence] 

        # Return index of first occurrance of True
        return self_list.index(True) < other_list.index(True)

    def sort_children(self):
        # Children need to prefer literals before identifiers
        # Otherwise, generic definitions may match before
        # a more specific definition
        self.children = sorted(self.children)
    
    def add_child(self, node):
        self.children.append(node)
        self.sort_children()
    
    def merge(self, other):
        # Update parent ref for other children
        for child_node in other.children:
            child_node.parent = self

        # Combine children from other node
        self.children.extend(other.children)
        self.sort_children()

class FunctionLookupTree(object):
    def __init__(self, parameters, expressions):
        self.root = FunctionLookupNode()

        # Create a chain of parameters
        # This chain can be merged later
        # And will be used for function lookup
        def create_path(node, parameters):
            if len(parameters) == 0:
                node.expressions = expressions
                return node
            for param in parameters:
                return create_path(
                    node=FunctionLookupNode(param, node),
                    parameters=parameters[1:]
                )

        # Start the recursion
        create_path(self.root, parameters)
    
    def find(self, arguments, scope, current_node=None):
        current_node = get_default(current_node, self.root)

        if len(arguments) == 0:
            return current_node.expressions

        # If there are no children, put rest of arguments in
        # ...rest if the rest operator is used

        for lookup_node in current_node.children:
            identifier_match = isinstance(lookup_node.parameter, AST.Identifier)
            literal_match = isinstance(lookup_node.parameter, AST.Literal) and lookup_node.parameter.value == arguments[0]

            if identifier_match or literal_match:
                # Make sure this scales when we add multiple params
                # Always match with remaining argument list, not next index
                if identifier_match:
                    scope[lookup_node.parameter.value] = arguments[0]

                if(lookup_node.expressions is None):
                    find_result = self.find(arguments[1:], scope, current_node=lookup_node) 
                    if find_result is not None:
                        return find_result
                    else:
                        continue
                else:
                    return lookup_node.expressions
    
    def merge(self, other):
        if len(self.root.children) > 0 and len(other.root.children):
            self.root.merge(other.root)

        return self

class Builtins(object):
    @staticmethod
    def func_add(args):
        return reduce(operator.add, args)

    @staticmethod
    def func_mul(args):
        return reduce(operator.mul, args)

    @staticmethod
    def func_minus(args):
        if len(args) == 1:
            return args[0] * -1
        return args[0] - args[1]

    @staticmethod
    def func_eq(args):
        if(args[0] == args[1]):
            return True
        return False

    @staticmethod
    def func_if(args):
        if len(args)>3:
           raise Exception('Too many arguments for if. expected < 3')
        elif len(args) == 3:
            if args[0]:
                return args[1]
            else:
                return args[2]
        

built_in_functions = {
    '*': Builtins.func_mul,
    '+': Builtins.func_add,
    '-': Builtins.func_minus,
    'if': Builtins.func_if,
    '==': Builtins.func_eq
}

class Scope(dict):
    def __init__(self, dictionary=None, parent=None, exclude_built_ins=False):
        if dictionary is None:
            dictionary = {}

        self.dictionary = dictionary

        # If this is the root node, add the built-in functions
        if(parent is None and not exclude_built_ins):
            self.dictionary.update(built_in_functions)

        # Refer to the parent in the hierarchy
        self.parent = parent
    
    def create_child(self, dictionary=None):
        # Create shallow copy of a parent scope
        return Scope(
            dictionary=get_default(dictionary, {}),
            parent=self
        )

    def define_function(self, ast_function):
        identifier = ast_function.identifier.value
        parameters = ast_function.parameters
        expressions = ast_function.expressions
        # use identifier and params so that lookup works from args
        if identifier in self:
            self[identifier].merge(FunctionLookupTree(parameters, expressions))
        else:
            self[identifier] = FunctionLookupTree(parameters, expressions)

    def lookup_function(self, function_identifier, function_arguments):
        if function_identifier in self:
            match = self[function_identifier]
            if type(match) == FunctionLookupTree:
                return match.find(function_arguments, scope=self)
            else:
                return AST.IntegerLiteral(match(function_arguments))

    def __contains__(self, key):
        if key in self.dictionary:
            return True
        elif self.parent != None:
            return self.parent.__contains__(key)
        else:
            return False
    
    def __setitem__(self, key, value):
        self.dictionary[key] = value

    def __getitem__(self, key):
        if key in self.dictionary:
            return self.dictionary[key]
        elif self.parent is not None:
            return self.parent.__getitem__(key)

        # Throw the built in error if we can't find the element
        # this will actually only get executed on the root global scope
        return self.dictionary[key]

    def __eq__(self, other):
        return self.dictionary == other

    def __repr__(self):
        return 'Scope:' + self.dictionary.__repr__() + 'Parent: ' + self.parent.__repr__()

# class Closure(dict):
#     def __init__(self):
#         pass
