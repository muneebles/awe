from functools import reduce
from rply import ParserGenerator
from rply.token import BaseBox
import operator
from Tokenizer import tokenize, tokens
import AbstractSyntaxTree as AST

# Create a parser generator instance
# We'll use this later to create a parser instance
pg = ParserGenerator(tokens, cache_id="myparser")

def handle_production_list(p):
    if(len(p) == 1):
        # return single argument as list
        return [p[0]]
    # append additional arguments to the existing list
    p[0].append(p[1])
    return p[0]

@pg.production("main : expr_list")
def main(p):
    return AST.ExpressionList(
        expressions=p[0]
    )

# *********************************************
# EXPRESSIONS
# *********************************************
@pg.production("expr_list : expr_list expr")
@pg.production("expr_list : expr")
def expr_list(p):
    return handle_production_list(p)

@pg.production("expr : OPEN_PAREN operator argument_list CLOSE_PAREN")
@pg.production("expr : OPEN_PAREN keyword identifier function_definition_list CLOSE_PAREN")
@pg.production("expr : OPEN_PAREN identifier argument_list CLOSE_PAREN")
@pg.production("expr : OPEN_PAREN identifier CLOSE_PAREN")
@pg.production("expr : OPEN_PAREN literal CLOSE_PAREN")
def expr(p):
    if len(p) == 3:
        if isinstance(p[1], AST.Identifier):
            return AST.Expression(function=p[1])
        else:
            return AST.Expression(arguments=[p[1]])
    elif type(p[1]) == AST.Keyword:
        # give things names
        function_name = p[2]
        function_list = p[3]
        # build argument list for defn
        arguments = [function_name]
        arguments.extend(function_list)
        # return expression for defn
        return AST.Expression(
            function=p[1],
            arguments=arguments
        )

    else:
        return AST.Expression(function=p[1], arguments=p[2])

# *********************************************
# FUNCTION DEFINITIONS
# *********************************************
@pg.production("function_definition_list : function_definition_list function_definition")
@pg.production("function_definition_list : function_definition")
def function_definition_list(p):
    return handle_production_list(p)

@pg.production("function_definition : OPEN_SQUARE_PAREN CLOSE_SQUARE_PAREN literal")
@pg.production("function_definition : OPEN_SQUARE_PAREN parameter_list CLOSE_SQUARE_PAREN literal")
@pg.production("function_definition : OPEN_SQUARE_PAREN CLOSE_SQUARE_PAREN expr_list")
@pg.production("function_definition : OPEN_SQUARE_PAREN parameter_list CLOSE_SQUARE_PAREN expr_list")
def function_definition(p):
    if isinstance(p[-1], AST.Literal):
        # Convert literal to expression list
        p[-1] = [AST.Expression(
            arguments=[p[-1]]
        )]
    if len(p) == 3:
        return AST.Function(expressions=p[2])
    return AST.Function(
        parameters=p[1],
        expressions=p[3]
    )

# *********************************************
# PARAMETERS
# *********************************************
@pg.production("parameter_list : parameter_list parameter")
@pg.production("parameter_list : parameter")
def parameter_list(p):
    return handle_production_list(p)

@pg.production("parameter : literal")
@pg.production("parameter : identifier")
def parameter(p):
    return p[0]

# *********************************************
# ARGUMENTS
# *********************************************
@pg.production("argument_list : argument_list argument")
@pg.production("argument_list : argument")
def argument_list(p):
    return handle_production_list(p)

@pg.production("argument : identifier")
@pg.production("argument : literal")
@pg.production("argument : expr")
def argument(p):
    return p[0]


# *********************************************
# LITERALS
# *********************************************
@pg.production("operator : OPERATOR")
def keyword(p):
    return AST.Operator(p[0].getstr())

@pg.production("keyword : KEYWORD")
def keyword(p):
    return AST.Keyword(p[0].getstr())

@pg.production("identifier : IDENTIFIER")
def identifier(p):
    return AST.Identifier(p[0].getstr())

@pg.production("literal : dictionary_literal")
@pg.production("literal : array_literal")
@pg.production("literal : string_literal")
@pg.production("literal : integer_literal")
@pg.production("literal : boolean")
def literal(p):
    return p[0]

@pg.production("boolean : BOOLEAN")
def keyword(p):
    return AST.Boolean(p[0].getstr())

@pg.production("string_literal : STRING")
def string_literal(p):
    return AST.StringLiteral(p[0].getstr()[1:-1])

@pg.production("integer_literal : NUMBER")
def integer_literal(p):
    return AST.IntegerLiteral(p[0].getstr())

@pg.production("array_literal : OPEN_SQUARE_PAREN CLOSE_SQUARE_PAREN")
def array_literal(p):
    return AST.ArrayLiteral()

@pg.production("dictionary_literal : OPEN_CURLY_PAREN CLOSE_CURLY_PAREN")
@pg.production("dictionary_literal : OPEN_CURLY_PAREN dict_pair_list CLOSE_CURLY_PAREN")
def dictionary_literal(p):
    # values have been specified
    if(len(p) == 3):
        dictionary = {}
        for pair in p[1]:
            dictionary[pair.key]=pair.value
        return AST.DictionaryLiteral(dictionary)

    # empty dictionary literal
    return AST.DictionaryLiteral()

@pg.production("dict_pair_list : dict_pair_list dict_pair")
@pg.production("dict_pair_list : dict_pair")
def dictionary_pair_list(p):
    return handle_production_list(p)

@pg.production("dict_pair : STRING COLON STRING")
def dictionary_pair(p):
    return AST.DictionaryKeyValuePair(
        key=p[0].getstr()[1:-1],
        value=p[2].getstr()[1:-1]
    )

@pg.error
def error_handler(token):
    raise SyntaxError("Invalid characters encountered: {errToken}".format(
        errToken=token
    ))

# Create a parser instance to be shared across calls to parse
# Builds Abstract Syntax Tree from token stream
parser = pg.build()

# Public interface, should be used to actually parse source
# TODO: need to hide the other functions from importers of this module
def parse(source):
    result = tokenize(source)
    return parser.parse(tokenize(source))

# Example in awe of this parse function
#
# evaluate = (defn parse -> List<Token>
#                [source:String] (parser.parse (lexer.lex source)))
#
