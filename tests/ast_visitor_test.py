import unittest
import AbstractSyntaxTree as AST
from Scope import *

class InvalidVisitor(AST.AstNode):
    pass

class ValidVisitor(AST.AstNode):
    def __visit__(self, scope=Scope()):
        return 'I am the return value'

class AST_Visitor_Test(unittest.TestCase):
    def test_valid_visit_function(self):
        # Check that the actual value
        self.assertEqual(ValidVisitor().visit(scope=Scope()), 'I am the return value')

    def test_invalid_visit_function(self):
        self.assertRaises(NotImplementedError, InvalidVisitor().visit, Scope())
