import unittest
from Tokenizer import tokenList

class Syntax_Test(unittest.TestCase):
	def test_empty_program (self):
		source = ""
		expected = []
		self.assertEqual(tokenList(source), expected)

	def test_paren(self):
		source = "()"
		expected = [
			'OPEN_PAREN',
			'CLOSE_PAREN'
		]
		self.assertEqual(tokenList(source), expected)

	def test_number_literals(self):
		source = "(2)"
		expected = [
			'OPEN_PAREN',
			'NUMBER',
			'CLOSE_PAREN'
		]
		self.assertEqual(tokenList(source), expected)

	def test_string_literals(self):
		source = "('hey')"
		expected = [
			'OPEN_PAREN',
			'STRING',
			'CLOSE_PAREN'
		]
		self.assertEqual(tokenList(source), expected)

	def test_identifiers(self):
		source = "(hey)"
		expected = [
			'OPEN_PAREN',
			'IDENTIFIER',
			'CLOSE_PAREN'
		]
		self.assertEqual(tokenList(source), expected)

	def test_identifiers_function_call(self):
		source = "(something 1 3)"
		expected = [
			'OPEN_PAREN',
			'IDENTIFIER',
			'NUMBER',
			'NUMBER',
			'CLOSE_PAREN'
		]
		self.assertEqual(tokenList(source), expected)

	def test_operators(self):
		operators = ['+', '-', '/', '*', '==', '!=']
		source = '(' + ')('.join(operators) + ')'
		expected = ['OPEN_PAREN', 'OPERATOR', 'CLOSE_PAREN'] * len(operators)
		self.assertEqual(tokenList(source), expected)

	def test_operator_expression(self):
		operators = ['+', '-', '/', '*', '==', '!=']
		source = " ".join(operators)
		expected = ['OPERATOR'] * len(operators)
		self.assertEqual(tokenList(source), expected)

	def test_curly_braces(self):
		source = "{}"
		expected = [
			'OPEN_CURLY_PAREN',
			'CLOSE_CURLY_PAREN'
		]
		self.assertEqual(tokenList(source), expected)

	def test_dictionary_literal(self):
		source = "({'name':'Muneeb'})"
		expected = [
			'OPEN_PAREN',
			'OPEN_CURLY_PAREN',
			'STRING',
			'COLON',
			'STRING',
			'CLOSE_CURLY_PAREN',
			'CLOSE_PAREN'
		]
		self.assertEqual(tokenList(source), expected)

	def test_square_braces(self):
		source = "[]"
		expected = [
			'OPEN_SQUARE_PAREN',
			'CLOSE_SQUARE_PAREN'
		]
		self.assertEqual(tokenList(source), expected)

	def test_keywords(self):
		keywords = ['let', 'fn', 'defn']
		source = " ".join(keywords)
		expected = ['KEYWORD'] * len(keywords)
		self.assertEqual(tokenList(source), expected)

	def test_keywords_not_identifiers(self):
		keywords = ['let', 'lets', 'slet', 'fn', 'fne', 'efn']
		source = " ".join(keywords)
		expected = [
			'KEYWORD',
			'IDENTIFIER',
			'IDENTIFIER',
			'KEYWORD',
			'IDENTIFIER',
			'IDENTIFIER'
		]
		self.assertEqual(tokenList(source), expected)

	def test_conditional_expr(self):
		actual = tokenList('(if true 1 2)')
		expected = [
			'OPEN_PAREN',
			'IDENTIFIER',
			'BOOLEAN',
			'NUMBER',
			'NUMBER',
			'CLOSE_PAREN'
		]
		self.assertEqual(actual, expected)

	def test_function_with_params(self):
		actual = tokenList(
			"""
			(defn test 
				[n] (+ 1 2))

			(test)
			"""
		)
		expected = [
			'OPEN_PAREN',
			'KEYWORD',
			'IDENTIFIER',
			'OPEN_SQUARE_PAREN',
			'IDENTIFIER',
			'CLOSE_SQUARE_PAREN',
			'OPEN_PAREN',
			'OPERATOR',
			'NUMBER',
			'NUMBER',
			'CLOSE_PAREN',
			'CLOSE_PAREN',
			'OPEN_PAREN',
			'IDENTIFIER',
			'CLOSE_PAREN'
		]
		self.assertEqual(actual, expected)