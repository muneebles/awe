import unittest
from Parser import parse
import AbstractSyntaxTree as AST
import operator
class AST_Node_Test(unittest.TestCase):
	def test_string_literal(self):
		self.assertEqual(AST.StringLiteral("A").value, "A")
		self.assertEqual(AST.StringLiteral(5).value, "5")

	def test_string_literal_equality(self):
		self.assertEqual(AST.StringLiteral(2), AST.StringLiteral(2))
		self.assertNotEqual(AST.StringLiteral(1), AST.StringLiteral(2))
	
	def test_integer_literal(self):
		self.assertEqual(AST.IntegerLiteral(3).value, 3)
		self.assertEqual(AST.IntegerLiteral("2").value, 2)

	def test_integer_literal_equality(self):
		self.assertEqual(AST.IntegerLiteral(2), AST.IntegerLiteral(2))
		self.assertNotEqual(AST.IntegerLiteral(1), AST.IntegerLiteral(2))

	def test_expression(self):
		self.assertEqual(AST.Expression().arguments, [])
		self.assertEqual(AST.Expression().function, AST.Identifier('identity'))

	def test_expression_equality(self):
		self.assertEqual(AST.Expression(), AST.Expression())
		self.assertNotEqual(AST.Expression(), AST.Expression(arguments=[1]))

	def test_expression_list(self):
		self.assertEqual(AST.ExpressionList().expressions, [])
		self.assertEqual(AST.ExpressionList(expressions=[]).expressions, [])

	def test_expression_list_equality(self):
		self.assertEqual(
			AST.ExpressionList(expressions=[AST.Expression()]),
			AST.ExpressionList(expressions=[AST.Expression()])
		)
		self.assertNotEqual(
			AST.ExpressionList(expressions=[AST.Expression()]),
			AST.ExpressionList()
		)