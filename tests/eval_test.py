import unittest
from Interpreter import Interpreter

class Evaluation_Test(unittest.TestCase):
    def test_string_literal_evaluation(self):
        actual, result_scope = Interpreter.evaluate("('a')")
        expected = 'a'
        self.assertEqual(actual, expected)

    def test_integer_literal_evaluation(self):
        actual, result_scope = Interpreter.evaluate('(1)')
        expected = 1
        self.assertEqual(actual, expected)

    def test_builtin_function_call(self):
        actual, result_scope = Interpreter.evaluate('(+ 1 2 4)')
        expected = 7
        self.assertEqual(actual, expected)

    def test_another_builtin_function_call(self):
        actual, result_scope = Interpreter.evaluate('(* 2 3 4)')
        expected = 24
        self.assertEqual(actual, expected)

    def test_nested_expressions(self):
        actual, result_scope = Interpreter.evaluate('(* 2 3 (+ 1 2))')
        expected = 18
        self.assertEqual(actual, expected)

    def test_function_definition(self):
        actual, result_scope = Interpreter.evaluate(
            """
            (defn one
                [] (1))
            """
        )
        expected = None
        self.assertEqual(actual, expected)

    def test_function_call(self):
        actual, result_scope = Interpreter.evaluate(
            """
            (defn one [] 1)
            (one)
            """
        )
        expected = 1
        self.assertEqual(actual, expected)

    def test_interpreter_stateless(self):
        definition = Interpreter.evaluate(
            """
            (defn one [] (1))
            """
        )
        usage, result_scope = Interpreter.evaluate('(one)')
        self.assertEqual(usage, None)

    def test_true_condition(self):
        actual, _ = Interpreter.evaluate(
            """
            (if true 1 2)
            """
        )
        expected = 1
        self.assertEqual(actual, expected)

    def test_false_condition(self):
        actual, _ = Interpreter.evaluate(
            """
            (if false 1 2)
            """
        )
        expected = 2
        self.assertEqual(actual, expected)

    def test_true_expression_condition(self):
        actual, _ = Interpreter.evaluate(
            """
            (if true (+ 1 2) (+ 3 4))
            """
        )
        expected = 3
        self.assertEqual(actual, expected)

    def test_false_expression_condition(self):
        actual, _ = Interpreter.evaluate(
            """
            (if (== 1 2) 1 2)
            """
        )
        expected = 2
        self.assertEqual(actual, expected)

    def test_boolean_evaulation(self):
        actual, _ = Interpreter.evaluate(
            """
            (if (== 1 1) 1 2)
            """
        )
        expected = 1
        self.assertEqual(actual, expected)

    def test_function_parameters_defined(self):
        actual, _ = Interpreter.evaluate(
            """
            (defn test 
                [n] (+ 1 2))

            (test 6)
            """
        )
        expected = 3
        self.assertEqual(actual, expected)

    def test_function_parameters_used(self):
        actual, _ = Interpreter.evaluate(
            """
            (defn test 
                [n] (+ 1 n))

            (test 5)
            """
        )
        expected = 6
        self.assertEqual(actual, expected)

    def test_recursion(self):
        actual, _ = Interpreter.evaluate(
            """
            (defn fact
                [n] (if (== 0 n)
                        1
                        (* n (fact (- n 1)))))

            (fact 5)
            """
        )
        expected = 120
        self.assertEqual(actual, expected)
    
    def test_literal_params(self):
        _, scope = Interpreter.evaluate(
            """
            (defn literal
                [0] 1
                [n] 2)
            """
        )
        actual1, _ = Interpreter.evaluate("(literal 0)", scope=scope)
        self.assertEqual(actual1, 1)
        actual2, _ = Interpreter.evaluate("(literal 5)", scope=scope)
        self.assertEqual(actual2, 2)

    def test_literal_factorial(self):
        actual, _ = Interpreter.evaluate(
            """
            (defn fact
                [0] 1
                [n] (* n (fact (- n 1))))

            (fact 5)
            """
        )
        expected = 120
        self.assertEqual(actual, expected)

    def test_multiple_parameters(self):
        actual, _ = Interpreter.evaluate(
            """
            (defn add
                [a b] (+ a b))

            (add 1 3)
            """
        )
        expected = 4
        self.assertEqual(actual, expected)

    def test_multiple_literal_parameters(self):
        _, scope = Interpreter.evaluate(
            """
            (defn add
                [1 6] 7
                [2 5] 8)
            """
        )
        actual1, _ = Interpreter.evaluate("(add 1 6)", scope=scope)
        self.assertEqual(actual1, 7)
        actual2, _ = Interpreter.evaluate("(add 2 5)", scope=scope)
        self.assertEqual(actual2, 8)

    def test_multi_literal_multi_identifier_parameters(self):
        _, scope = Interpreter.evaluate(
            """
            (defn add
                [1 6] 7
                [a b] 8)
            """
        )
        actual1, _ = Interpreter.evaluate("(add 1 6)", scope=scope)
        self.assertEqual(actual1, 7)
        actual2, _ = Interpreter.evaluate("(add 1 5)", scope=scope)
        self.assertEqual(actual2, 8)

    def test_literal_param_prioritization(self):
        _, scope = Interpreter.evaluate(
            """
            (defn add
                [a] 7
                [1] 8)
            """
        )
        actual2, _ = Interpreter.evaluate("(add 1)", scope=scope)
        self.assertEqual(actual2, 8)