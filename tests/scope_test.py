import unittest
from Scope import Scope 

class Scope_Test(unittest.TestCase):
    def test_scope(self):
        actual = Scope(exclude_built_ins=True)
        expected = {}
        self.assertEqual(actual, expected)
    
    def test_scope_with_dict(self):
        dictionary = {
            'one': 1,
            'two': 2
        }
        actual = Scope(dictionary, exclude_built_ins=True)
        self.assertEqual(actual, dictionary)

    def test_scope_get(self):
        parent = Scope({'a':1})
        self.assertEqual(parent['a'], 1)

    def test_scope_set(self):
        parent = Scope({'a':1}, exclude_built_ins=True)
        parent['b'] = 2
        self.assertEqual(parent, {'a':1, 'b':2})
    
    def test_parent_scope_get(self):
        parent = Scope({'a': 1}, exclude_built_ins=True)
        child = parent.create_child({'b':2})
        self.assertEqual(child['a'], 1)
        self.assertEqual(child['b'], 2)
        self.assertEqual(parent['a'], 1)
        self.assertRaises(KeyError, lambda: parent['b'])

    def test_child_has_parent_ref(self):
        # Allows late binding, basically.
        # Design decision? static checks may be impacted
        # Is it possible to determine if it will ever be defined?
        parent = Scope({'a': 1}, exclude_built_ins=True)
        child = parent.create_child({'b':2})
        self.assertRaises(KeyError, lambda: child['c'])
        parent['c'] = 3
        self.assertEqual(child['c'], 3)
    
    def test_in_operator(self):
        scope = Scope({'a': 1})
        self.assertEqual('b' in scope, False)
        self.assertEqual('a' in scope, True)
    
    def test_built_ins(self):
        # test that the full list of built in functions exist
        self.assertTrue('+' in Scope())
        self.assertTrue('*' in Scope())
        pass
